<form method="POST" action="#">
  <label for="amount">Amount:</label>
  <input type="number" name="amount" id="amount" required>
  <br>

  <label for="senderName">Sender Name:</label>
  <input type="text" name="senderName" id="senderName" required>
  <br>

  <label for="senderStreet">Sender Street:</label>
  <input type="text" name="senderStreet" id="senderStreet" required>
  <br>

  <label for="senderPlace">Sender Place:</label>
  <input type="text" name="senderPlace" id="senderPlace" required>
  <br>

  <label for="receiverName">Receiver Name:</label>
  <input type="text" name="receiverName" id="receiverName" required>
  <br>

  <label for="receiverStreet">Receiver Street:</label>
  <input type="text" name="receiverStreet" id="receiverStreet" required>
  <br>

  <label for="receiverPlace">Receiver Place:</label>
  <input type="text" name="receiverPlace" id="receiverPlace" required>
  <br>

  <label for="iban">IBAN:</label>
  <input type="text" name="iban" id="iban" required>
  <br>

  <label for="model">Model:</label>
  <input type="text" name="model" id="model" required>
  <br>

  <label for="reference">Reference:</label>
  <input type="text" name="reference" id="reference" required>
  <br>

  <label for="purpose">Purpose:</label>
  <input type="text" name="purpose" id="purpose" required>
  <br>

  <label for="description">Description:</label>
  <input type="text" name="description" id="description" required>
  <br>

  <button type="submit">Submit</button>
</form>
<?php
// ulazni JSON za zahtjev
$json = '{
    "renderer": "image",
    "options": {
        "format": "png",
        "color": "#000000"
    },
    "data": {
        "amount": 100000,
        "sender": {
            "name": "Ivan Habunek",
            "street": "Savska cesta 13",
            "place": "10000 Zagreb"
        },
        "receiver": {
            "name": "Big Fish Software d.o.o.",
            "street": "Savska cesta 13",
            "place": "10000 Zagreb",
            "iban": "HR6623400091110651272",
            "model": "00",
            "reference": "123-456-789"
        },
        "purpose": "ANTS",
        "description": "Developing a HUB-3 API"
    }
}';

// postavljanje opcija za HTTP zahtjev
$options = array(
    'http' => array(
        'header'  => "Content-type: application/json\r\n",
        'method'  => 'POST',
        'content' => $json,
    ),
);

// slanje HTTP zahtjeva na servis
$context  = stream_context_create($options);
$response = file_get_contents('https://hub3.bigfish.software/api/v1/barcode', false, $context);

file_put_contents('slika.png', $response);
    // prikazivanje slike na ekranu
    echo '<img src="data:image/png;base64,' . base64_encode($response) . '">';
// provjera da li je dobiven odgovor u obliku slike
echo "kako " . $http_response_header[0]; 
echo "ovako ".  $http_response_header[2]; 
if (strpos($http_response_header[0], '200') !== false && strpos($http_response_header[2], 'image/png') !== false) {
    // spremanje slike u datoteku
    file_put_contents('slika.png', $response);
    // prikazivanje slike na ekranu
    echo '<img src="data:image/png;base64,' . base64_encode($response) . '">';
} else {
    // neuspjeh, greška pri dohvatu slike
    echo 'Došlo je do greške prilikom dohvata slike.';
}
?>